package com.example.jsonparsingurlkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import org.json.JSONException
import org.json.JSONObject
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.extension.responseJson
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    internal var URL = "https://demonuts.com/Demonuts/JsonTest/Tennis/json_parsing.php"

//    internal var URL = "https://corona.lmao.ninja/v2/countries"


    private val jsoncode = 1
    private var listView: ListView? = null
    private var playersModelArrayList: ArrayList<PlayersModel>? = null
    private var customeAdapter: CustomeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.lv) as ListView

        sampleKo()


    }

    private fun sampleKo() {
   //https://github.com/androidmads/KotlinFuelHttpSample/blob/master/app/src/main/java/com/androidmads/kotlinfuelhttpsample/MainActivity.kt
        //https://github.com/kittinunf/Fuel/blob/1.16.0/README.md
        try {

            Fuel.post(URL, listOf()).responseJson { request, response, result ->
                Log.d("plzzzzz", result.get().content)
                onTaskCompleted(result.get().content)
            }
        } catch (e: Exception) {

        } finally {

        }
    }


    fun onTaskCompleted(response: String) {
        Log.d("responsejson", response)

        playersModelArrayList = getInfo(response)
        customeAdapter = CustomeAdapter(this, playersModelArrayList!!)
        listView!!.adapter = customeAdapter
    }

    fun getInfo(response: String): ArrayList<PlayersModel> {
        val playersModelArrayList = ArrayList<PlayersModel>()
        try {
            val jsonObject = JSONObject(response)
            println("Response is .....",jsonObject)
            if (jsonObject.getString("status") == "true") {

                val dataArray = jsonObject.getJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    val playersModel = PlayersModel()
                    val dataobj = dataArray.getJSONObject(i)
                    playersModel.setNames(dataobj.getString("name"))
                    playersModel.setCountrys(dataobj.getString("country"))
                    playersModel.setCitys(dataobj.getString("city"))
                    playersModelArrayList.add(playersModel)
                }
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return playersModelArrayList
    }

    private fun println(s: String, jsonObject: JSONObject) {

    }

}


